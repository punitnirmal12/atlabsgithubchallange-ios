//
//  WSHome.swift
//  AtlabsGithubChallange
//
//  Created by Punit Nirmal on 19/02/21.
//  Copyright © 2021 Punit Nirmal. All rights reserved.
//

import UIKit
import ObjectMapper
class WSHome: NSObject {
    /*
     Get user details based on username passed in argument and send result back
    */
    static func getGithubUser(name:String,successHandler: @escaping (_ response: MUser) -> Void,errorHandler: @escaping (_ error: Error) -> Void){
        NetworkManager.makeJSONObjectRequest(HttpRouter.getGithubUser(name: name),showProgress: true).onSuccess { (response) in
            print("gitUserResponse : ",response)
            if let gitUserResponse = Mapper<MUser>().map(JSON: response as! [String : Any]) {
                successHandler(gitUserResponse)
            }
            
        }.onFailure { (error) in
            errorHandler(error)
        }
    }
}
