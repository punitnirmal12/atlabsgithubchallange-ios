//
//  HomeVC.swift
//  AtlabsGithubChallange
//
//  Created by Punit Nirmal on 19/02/21.
//  Copyright © 2021 Punit Nirmal. All rights reserved.
//

import UIKit
import SDWebImage

class HomeVC: UIViewController {

    @IBOutlet weak var txtSearch: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewSearch: UIView!
    var arrMembers:[MUser] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        Networks.shared.startNetworkReachabilityObserver()
    }
    override func viewWillAppear(_ animated: Bool) {
        tableView.separatorStyle = .none
        if arrMembers.count == 0 {
            viewSearch.isHidden = false
        }else{
            viewSearch.isHidden = true
        }
        addObservers()
    }
    override func viewWillDisappear(_ animated: Bool) {
        removeObservers()
    }
    /*
     To add observer for network change
    */
    func addObservers(){
        NotificationCenter.default.addObserver(
        self,
        selector: #selector(self.networkChange),
        name: NSNotification.Name(rawValue: NETWORK_CHANGE),
        object: nil)
    }
    /*
     To remove observer for network change
    */
    func removeObservers(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NETWORK_CHANGE), object: nil)
    }
    /*
     To perform task when network change is detected
    */
    @objc func networkChange(notification: Notification) {
        print("received net check: ",notification)
        let userinfo = notification.userInfo
        let isNetwork = userinfo!["isNetwork"] as! Bool
        if !isNetwork {
             let noNetVC = storyboard?.instantiateViewController(withIdentifier: "NoNetworkVC") as! NoNetworkVC
             self.navigationController?.pushViewController(noNetVC, animated: true)
        }
    }
}
extension HomeVC:UITableViewDelegate,UITableViewDataSource{
    /*
     tableview delegate methods
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMembers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell") as? HomeCell
        if cell == nil {
            let nib = Bundle.main.loadNibNamed("HomeCell", owner: self, options: nil)
            cell = nib?[0] as? HomeCell
        }
        cell?.selectionStyle = .none
    
        cell?.img_avatar.sd_setImage(with: URL.init(string: arrMembers[indexPath.row].avatar_url ?? ""), placeholderImage: UIImage.init(named: ""), options: SDWebImageOptions.continueInBackground, context: nil)
        cell?.lblName.text = arrMembers[indexPath.row].name ?? "Name Not Available"
        cell?.lblUsername.text = arrMembers[indexPath.row].login ?? "Username not Available"
        cell?.lblBio.text = arrMembers[indexPath.row].bio ?? "Bio not Available"
        cell?.lblRepoCount.text = "\(arrMembers[indexPath.row].public_repos ?? 0)"
        cell?.lblFollowingCount.text = "\(arrMembers[indexPath.row].following ?? 0)"
        cell?.lblFollowersCount.text = "\(arrMembers[indexPath.row].followers ?? 0)"
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let repoVC = storyboard?.instantiateViewController(withIdentifier: "RepoVC") as! RepoVC
        
        repoVC.mUSer = arrMembers[indexPath.row]
        self.navigationController?.pushViewController(repoVC, animated: true)
    }
    
}
extension HomeVC:UISearchBarDelegate{
    /*
     searchbar delegate methods
    */
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("searchText \(searchText)")
        if searchText.count == 0{
            arrMembers.removeAll()
            tableView.reloadData()
            viewSearch.isHidden = false
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        print("searchText cancel : \(String(describing: searchBar.text))")
        arrMembers.removeAll()
        tableView.reloadData()
        viewSearch.isHidden = false
    }
    /*
     call api to get user details on click of search button
    */
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("searchText done : \(String(describing: searchBar.text))")
        if let searchText = searchBar.text {
            if searchText.count>0 {
                WSHome.getGithubUser(name: searchText, successHandler: { (mHome) in
                    self.arrMembers.append(mHome)
                    self.tableView.reloadData()
                    self.viewSearch.isHidden = true
                }) { (error) in
                    print(error)
                }
            }
            
        }
        
    }
}
