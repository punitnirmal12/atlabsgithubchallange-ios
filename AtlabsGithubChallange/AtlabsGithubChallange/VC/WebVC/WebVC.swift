//
//  WebVC.swift
//  AtlabsGithubChallange
//
//  Created by Punit Nirmal on 22/02/21.
//  Copyright © 2021 Punit Nirmal. All rights reserved.
//

import UIKit
import WebKit
class WebVC: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    var url:String?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        webView.navigationDelegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        let link = URL(string:url!)!
        let request = URLRequest(url: link)
        webView.load(request)
    }
}

extension WebVC : WKNavigationDelegate{
    /*
     Webview delegate methods
    */
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        RappleIndicator.start()
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        RappleIndicator.stop()
    }
}
