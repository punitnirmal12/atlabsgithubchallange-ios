//
//  RepoVC.swift
//  AtlabsGithubChallange
//
//  Created by Punit Nirmal on 20/02/21.
//  Copyright © 2021 Punit Nirmal. All rights reserved.
//

import UIKit
import SDWebImage
class RepoVC: UIViewController {

    @IBOutlet weak var viewUserInfo: UIView!
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var btnAvatar: UIButton!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var tvBio: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewPublicRepo: UIView!
    @IBOutlet weak var viewFollowing: UIView!
    @IBOutlet weak var viewFollowers: UIView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet weak var lblRepoCount: UILabel!
    @IBOutlet weak var lblFollowingCount: UILabel!
    @IBOutlet weak var lblFollowersCount: UILabel!
    var arrMrepo:[MRepo] = []
    var mUSer:MUser?
    @IBOutlet weak var scrollView: UIScrollView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        tableView.separatorStyle = .none
        imgAvatar.layer.cornerRadius = imgAvatar.frame.size.height/2
        imgAvatar.clipsToBounds = true
        btnAvatar.layer.cornerRadius = btnAvatar.frame.size.height/2
        btnAvatar.clipsToBounds = true

    }
    override func viewWillAppear(_ animated: Bool) {
        setupUserInfo()
        addObservers()
    }
    override func viewWillDisappear(_ animated: Bool) {
        removeObservers()
    }
    /*
     image click event to view
    */
    @IBAction func btnAvatarClick(_ sender: Any) {
        let webVC = self.storyboard?.instantiateViewController(withIdentifier: "WebVC") as! WebVC
        
        webVC.url = mUSer!.avatar_url ?? ""
        self.present(webVC, animated: true, completion: nil)
    }
}
extension RepoVC{
    /*
     To add observers for network change
    */
    func addObservers(){
        NotificationCenter.default.addObserver(
        self,
        selector: #selector(self.networkChange),
        name: NSNotification.Name(rawValue: NETWORK_CHANGE),
        object: nil)
    }
    /*
     To remove observers for network change
    */
    func removeObservers(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NETWORK_CHANGE), object: nil)
    }
    /*
     To perform task on network change
    */
    @objc func networkChange(notification: Notification) {
        print("received net check: ",notification)
        let userinfo = notification.userInfo
        let isNetwork = userinfo!["isNetwork"] as! Bool
        if !isNetwork {
             let noNetVC = storyboard?.instantiateViewController(withIdentifier: "NoNetworkVC") as! NoNetworkVC
             self.navigationController?.pushViewController(noNetVC, animated: true)
        }
    }
    /*
     To set user information
    */
    func setupUserInfo(){
        self.title = mUSer?.name ?? mUSer?.login
        ViewShadow.setShadow(view: viewUserInfo)
        ViewShadow.setShadow(view: viewPublicRepo)
        ViewShadow.setShadow(view: viewFollowers)
        ViewShadow.setShadow(view: viewFollowing)
        ViewShadow.setShadow(view: viewHeader)
        imgAvatar.sd_setImage(with: URL.init(string: mUSer!.avatar_url ?? ""), placeholderImage: UIImage.init(named: ""), options: SDWebImageOptions.continueInBackground, context: nil)
        lblName.text = mUSer?.name ?? "Name Not Available"
        lblUsername.text = mUSer?.login ?? "Username Not Available"
        tvBio.text = mUSer?.bio ?? "Bio Not Available"
        lblRepoCount.text = "\(mUSer?.public_repos ?? 0)"
        lblFollowingCount.text = "\(mUSer?.following ?? 0)"
        lblFollowersCount.text = "\(mUSer?.followers ?? 0)"
        getRepo()
    }
    /*
     To get user's repository
    */
    func getRepo(){
        WSRepo.getGithubUserRepo(name: (mUSer?.login)!, successHandler: { (arrRepo) in
            self.arrMrepo = arrRepo
            self.tableView.reloadData()
        }) { (error) in
            
        }
    }
}
extension RepoVC:UITableViewDelegate,UITableViewDataSource{
    /*
     tableview delegate methods
    */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMrepo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "RepoCell") as? RepoCell
        if cell == nil {
            let nib = Bundle.main.loadNibNamed("RepoCell", owner: self, options: nil)
            cell = nib?[0] as? RepoCell
        }
        cell?.selectionStyle = .none
        cell?.lblName.text = arrMrepo[indexPath.row].name ?? "Name Not Available"
        cell?.lblDescription.text = arrMrepo[indexPath.row].description ?? "Description Not Available"
        cell?.lblLanguage.text = arrMrepo[indexPath.row].language ?? "NA"
        cell?.lblCreatedAt.text = arrMrepo[indexPath.row].created_at ?? "NA"
        cell?.lblUpdatedAt.text = arrMrepo[indexPath.row].updated_at ?? "NA"
        cell?.lblPushedAt.text = arrMrepo[indexPath.row].pushed_at ?? "NA"
        cell?.lblIssueCounts.text = "\(arrMrepo[indexPath.row].open_issues ?? 0)"
        cell?.openUrl = {
            let webVC = self.storyboard?.instantiateViewController(withIdentifier: "WebVC") as! WebVC
            
            webVC.url = self.arrMrepo[indexPath.row].html_url
            self.present(webVC, animated: true, completion: nil)
        }
        return cell!
    }
    
}
extension RepoVC:UIScrollViewDelegate{
    /*
     To restrict horizontal scroll for view
    */
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }
}
