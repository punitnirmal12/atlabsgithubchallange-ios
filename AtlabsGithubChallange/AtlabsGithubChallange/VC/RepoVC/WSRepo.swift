//
//  WSRepo.swift
//  AtlabsGithubChallange
//
//  Created by Punit Nirmal on 20/02/21.
//  Copyright © 2021 Punit Nirmal. All rights reserved.
//

import UIKit
import ObjectMapper
class WSRepo: NSObject {
    /*
     To get user's repository and send details back
    */
    static func getGithubUserRepo(name:String,successHandler: @escaping (_ response: [MRepo]) -> Void,errorHandler: @escaping (_ error: Error) -> Void){
        NetworkManager.makeJSONObjectRequest(HttpRouter.getRepo(name: name),showProgress: true).onSuccess { (response) in
            print("gitUserResponse : ",response)
            let gitUserRepo = Mapper<MRepo>().mapArray(JSONArray: response as! [[String:Any]] )
            successHandler(gitUserRepo)
            
            
        }.onFailure { (error) in
            errorHandler(error)
        }
    }
}
