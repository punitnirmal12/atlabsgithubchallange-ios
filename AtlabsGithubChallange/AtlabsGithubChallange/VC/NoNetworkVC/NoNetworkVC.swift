//
//  NoNetworkVC.swift
//  AtlabsGithubChallange
//
//  Created by Punit Nirmal on 23/02/21.
//  Copyright © 2021 Punit Nirmal. All rights reserved.
//

import UIKit
class NoNetworkVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        addObservers()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        removeObservers()
    }
    /*
     To add observer for network change
    */
    func addObservers(){
        NotificationCenter.default.addObserver(
        self,
        selector: #selector(self.networkChange),
        name: NSNotification.Name(rawValue: NETWORK_CHANGE),
        object: nil)
    }
    /*
     To remove observer for network change
    */
    func removeObservers(){
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NETWORK_CHANGE), object: nil)
    }
    /*
     Task to perform once network change is detected
    */
    @objc func networkChange(notification: Notification) {
        print("received net check: ",notification)
        let userinfo = notification.userInfo
        let isNetwork = userinfo!["isNetwork"] as! Bool
        if isNetwork {
             self.navigationController?.popViewController(animated: true)
        }
    }

}
