//
//  NetworkManager.swift
//  UXTesting
//


import Foundation
import BrightFutures
import ObjectMapper
import Alamofire
import AlamofireObjectMapper


// MARK: NetworkError
/*
 Enum for network error
*/
enum NetworkError: LocalizedError {
    case errorString(String)
    case errorModel(ErrorModel)
    case error(code: Double?, message: String)
    case generic
    var errorDescription: String? {
        switch self {
        case .errorString(let errorMessage):
            return errorMessage
        case .error(_, let message):
            return message
        case .errorModel(let errorMessage):
            if let error = errorMessage.error, !error.isEmpty {
                return error
            } else if let message = errorMessage.message, !message.isEmpty {
                return message
            }
            return GENERIC_ERROR_MESSAGE
        case .generic:
            return GENERIC_ERROR_MESSAGE
        }
        
    }
    
    var title: String {
        return APP_TITLE
    }
}

// MARK: ErrorModel
/*
 Error model
*/
class ErrorModel: Mappable {
    var error: String?
    var status: String?
    var message: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        error <- map["error"]
        status <- map["status"]
        message <- map["message"]
    }
}
/*
 NetworkManager is used to create and call API request. It will create API Request with the help of Almofire
*/
public struct NetworkManager {
    public init() {}
    public static let networkQueue = DispatchQueue(label: "\(String(describing: Bundle.main.bundleIdentifier)).networking-queue", attributes: .concurrent)
    
    /*
     API request call with JSON parameters
    */
    static func makeJSONObjectRequest(_ urlRequest: URLRequestConvertible,
                                      showProgress: Bool) -> Future<Any, NetworkError> {
        let promise = Promise<Any, NetworkError>()
        if !NetworkManager.isConnectionAvailable(title: APP_TITLE,
                                                 message: NO_INTERNET) {
            return promise.future
        }
        
        if (showProgress == true) {
            RappleIndicator.start()
        }
        
        
        let request = Alamofire.request(urlRequest)
            .validate()
            .responseJSON(queue: networkQueue) { response in
                print(response.result)
                print("\nResponse: \(NSString(data: response.data!, encoding: String.Encoding.utf8.rawValue)!)\n")
                
                if (showProgress == true) {
                    RappleIndicator.stop()
                }
                switch response.result {
                case .success:
                    if let error = error(fromResponseObject: response) {
                        promise.failure(error)
                    } else {
                        print("response.result.value : ",response.result.value as? Dictionary<String, Any> ?? ["":""])
                        promise.success((response.result.value) as Any)
                    }
                case .failure(let error):
                    promise.failure(generateError(from: error, with: response))
                }
        }
        debugPrint(request)
        return promise.future
    }
    
    /*
     To check the connection before API call
    */
    static func isConnectionAvailable(title: String = APP_TITLE, message: String) -> Bool {
        if let isRechable = NetworkReachabilityManager()?.isReachable, !isRechable {
            if let topVC = UIApplication.topViewController(), !topVC.isKind(of: UIAlertController.self) {
                topVC.showAlert(title: title, message: message , actionTitles: ["Ok"], actions:[
                    { action1 in }, nil])
            }
            return false
        }
        return true
    }
    
    static func error<T>(fromResponseObject responseObject: DataResponse<T>) -> NetworkError? {
        if let statusCode = responseObject.response?.statusCode {
            switch statusCode {
            case 200...201: return nil
            
            default:
                 if let data = responseObject.data {
                       do {
                        let jsonTemp = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
                           return NetworkError.generic
                       } catch let errorTemp as NSError {
                           return NetworkError.errorString(errorTemp.localizedDescription)
                       }
                 }
                else if let result = responseObject.result.value as? [String: Any], !result.isEmpty {
                    return NetworkError.generic
                } else if let data = responseObject.data {
                    do {
                        let jsonTemp = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                        
                        if let errorModel = Mapper<ErrorModel>().map(JSONObject: jsonTemp) {
                            return NetworkError.errorModel(errorModel)
                        }
                        return NetworkError.generic
                    } catch let errorTemp as NSError {
                        return NetworkError.errorString(errorTemp.localizedDescription)
                    }
                }
            }
        }
        return NetworkError.errorString(GENERIC_ERROR_MESSAGE)
    }
    
    
    /*
     Request failure errors
      - Parameters:
        - error: represents error description
        - responseObject: represents responseObject description
    */
    static func generateError<T>(from error: Error, with responseObject: DataResponse<T>) -> NetworkError {
        if  responseObject.response?.statusCode != nil {
            return NetworkManager.error(fromResponseObject: responseObject) ?? .generic
        } else {
            let code = (error as NSError).code
            switch code {
            case NSURLErrorNotConnectedToInternet, NSURLErrorCannotConnectToHost, NSURLErrorCannotFindHost:
                return NetworkError.error(code: 1000, message: NO_INTERNET)
            default:
                return NetworkError.errorString(GENERIC_ERROR_MESSAGE)
            }
        }
    }
}
