//
//  UXHttpRouter.swift
//  UXTesting
//

import Foundation
import Alamofire
import ObjectMapper
/*
 base url for API
*/
let baseURL = "https://api.github.com/"

/*
 enum to set API's name and request parameters. It also sets header based on need.
*/
public enum HttpRouter: URLRequestConvertible {
    /*
     list of API functions to be call
    */
    case getGithubUser(name:String)
    case getRepo(name:String)
    /*
     define method of the API call
    */
    var method: Alamofire.HTTPMethod {
        switch self {
        case .getGithubUser,
             .getRepo:
        return .get
            
        }
    }
    /*
     Map API name value
    */
    var path: String {
        switch self {
        case .getGithubUser(let username):
            return "users/\(username)"
        case .getRepo(let username):
            return "users/\(username)/repos"
        
        }
    }
    /*
     list methods which requires authorisation header
    */
    var isAuthorization: Bool {
        switch self {
        
        default:
            return false
        }
    }
    /*
     add url parameters if any
    */
    var urlParameters: [String: Any]? {
        switch self {
       
        default:
            return nil
        }
    }
    /*
     prepate Request
    */
    public func asURLRequest() throws -> URLRequest {
        let url = NSURL(string: baseURL + self.path)!
        var urlRequest = URLRequest(url: url as URL)
        urlRequest.httpMethod = method.rawValue
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
    
        
        switch self {
        
        case .getGithubUser,
             .getRepo:
            do {
                let jsonData: NSData = try JSONSerialization.data(withJSONObject: self.urlParameters ?? Dictionary(), options: JSONSerialization.WritingOptions.prettyPrinted) as NSData
                print("JSON URLParameters Request: \(NSString(data: jsonData as Data, encoding: String.Encoding.utf8.rawValue)! as String)")
            }
            catch let error as NSError {
                print("Could not prepare request: \(error), \(error.userInfo)")
            }
            
            return try URLEncoding.queryString.encode(urlRequest, with: self.urlParameters)
        
           
        }
        
        
        
    }
}
