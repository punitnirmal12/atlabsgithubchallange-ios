
import Foundation
import RappleProgressHUD
import Alamofire

var GENERIC_ERROR_MESSAGE = "There is an error! Please try again later."
var APP_TITLE = "Github Search"
var NO_INTERNET = "There is no network connectivity found. Please connect device with internet."
var NETWORK_CHANGE = "NETWORK_CHANGE"
/*
 Loader Start stop functions
*/
struct RappleIndicator {
    static func start(withStyle style: RappleStyle = .circle, message: String = "Loading...") {
        DispatchQueue.main.async() {
            let attributes = RappleActivityIndicatorView.attribute(style: style)
            RappleActivityIndicatorView.startAnimatingWithLabel(message, attributes: attributes)
        }
    }
    static func stop() {
        DispatchQueue.main.async() {
            RappleActivityIndicatorView.stopAnimation()
            
        }
    }
}

/*
 to get current active view controller
*/
extension UIApplication {
    class func topViewController(controller: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let navigationController = controller as? UINavigationController {
            return topViewController(controller: navigationController.visibleViewController)
        }
        
        if let presented = controller?.presentedViewController {
            return topViewController(controller: presented)
        }
        return controller
    }
}
/*
 To show alerts
*/
extension UIViewController {
    func showAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
        
    }
    
}
/*
 To add shadow to the view
*/
struct ViewShadow {
    static func setShadow(view:UIView){
        view.layer.cornerRadius = 5.0
        view.layer.shadowColor = UIColor.lightGray.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        view.layer.shadowRadius = 5.0
        view.layer.shadowOpacity = 0.7
    }
}
/*
 To check the network connectivity of the device
*/
class Networks {

//shared instance
static let shared = Networks()

let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")

func startNetworkReachabilityObserver() {

    reachabilityManager?.listener = { status in
        switch status {

            case .notReachable:
                print("The network is not reachable")
                NotificationCenter.default.post(name: Notification.Name(NETWORK_CHANGE), object: nil, userInfo: ["isNetwork":false])
                break;
            case .unknown :
                print("It is unknown whether the network is reachable")
                NotificationCenter.default.post(name: Notification.Name(NETWORK_CHANGE), object: nil, userInfo: ["isNetwork":false])
                break;
            case .reachable(.ethernetOrWiFi):
                print("The network is reachable over the WiFi connection")
                NotificationCenter.default.post(name: Notification.Name(NETWORK_CHANGE), object: nil, userInfo: ["isNetwork":true])
                break;
            case .reachable(.wwan):
                print("The network is reachable over the WWAN connection")
                NotificationCenter.default.post(name: Notification.Name(NETWORK_CHANGE), object: nil, userInfo: ["isNetwork":true])
                break;

            }
        }
        // start listening
        reachabilityManager?.startListening()
   }
    
}
