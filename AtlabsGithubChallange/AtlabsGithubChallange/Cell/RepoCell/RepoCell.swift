//
//  RepoCell.swift
//  AtlabsGithubChallange
//
//  Created by Punit Nirmal on 20/02/21.
//  Copyright © 2021 Punit Nirmal. All rights reserved.
//

import UIKit

class RepoCell: UITableViewCell {

    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblCreatedAt: UILabel!
    @IBOutlet weak var lblUpdatedAt: UILabel!
    @IBOutlet weak var lblPushedAt: UILabel!
    @IBOutlet weak var lblIssueCounts: UILabel!
    @IBOutlet weak var btnOpen: UIButton!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewButtonContainer: UIView!
    var openUrl : (()->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    /*
     To make UI changes for a cell
    */
    override func draw(_ rect: CGRect) {
        ViewShadow.setShadow(view: viewContainer)
        ViewShadow.setShadow(view: viewButtonContainer)
        viewButtonContainer.layer.cornerRadius = viewButtonContainer.frame.size.height/2
        viewButtonContainer.layer.borderWidth = 2.0
        viewButtonContainer.layer.borderColor = UIColor.init(red: 33.0/255.0, green: 68.0/255.0, blue: 111.0/255.0, alpha: 1.0).cgColor
        viewButtonContainer.clipsToBounds = true
        btnOpen.layer.cornerRadius = btnOpen.frame.size.height/2
        btnOpen.clipsToBounds = true
    }
    @IBAction func btnOpenUrlClick(_ sender: Any) {
        self.openUrl!()
    }
}
