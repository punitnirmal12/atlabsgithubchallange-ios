//
//  HomeCell.swift
//  AtlabsGithubChallange
//
//  Created by Punit Nirmal on 19/02/21.
//  Copyright © 2021 Punit Nirmal. All rights reserved.
//

import UIKit

class HomeCell: UITableViewCell {

    @IBOutlet weak var img_avatar: UIImageView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblUsername: UILabel!
    @IBOutlet weak var lblBio: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblRepoCount: UILabel!
    @IBOutlet weak var lblFollowingCount: UILabel!
    @IBOutlet weak var lblFollowersCount: UILabel!
    @IBOutlet weak var viewRepo: UIView!
    @IBOutlet weak var viewFollowing: UIView!
    @IBOutlet weak var viewFollowers: UIView!
    @IBOutlet weak var viewImage: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        img_avatar.layer.cornerRadius = img_avatar.frame.size.height/2
        viewImage.layer.cornerRadius = viewImage.frame.size.height/2
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    /*
     To make UI changes for a cell
    */
    override func draw(_ rect: CGRect) {
        ViewShadow.setShadow(view: viewContainer)
        ViewShadow.setShadow(view: viewRepo)
        ViewShadow.setShadow(view: viewFollowing)
        ViewShadow.setShadow(view: viewFollowers)
    }
    
}
